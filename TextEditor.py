"""
TextEditor.py
A graphical text editor

Author: Richard Harrington
Date Created: 9/4/2013
"""

from tkinter import *
import tkinter.filedialog as tkFileDialog

class TextEditor():
    root=Tk()
    text= Text(root)
    menubar = Menu(root)
    
    
    def askopenfile(self):
        # Open file dialog
        f=tkFileDialog.askopenfile()
        self.text.delete(0.0,END)
        self.text.insert(0.0,f.read())

    def asksavefile(self):
        # Save file dialog
        file=tkFileDialog.asksaveasfile()
        textoutput=self.text.get(0.0,END)
        file.write(textoutput.rstrip())
        file.write("\n")    

    def copy(self):
        # Copy selected text to clipboard
        self.root.clipboard_clear()
        self.root.clipboard_append(self.text.get(SEL_FIRST,SEL_LAST))

    def paste(self):
        # Paste text from clipboard
        try:
            self.text.insert(INSERT,self.root.selection_get(selection='CLIPBOARD'))
        except TclError:
            pass

    def cut(self):
        # Cut selected text from window
        self.root.clipboard_clear()
        self.root.clipboard_append(self.text.get(SEL_FIRST,SEL_LAST))
        self.text.delete(SEL_FIRST,SEL_LAST)

    def askexit(self):
        # Exit program
        self.root.destroy()

    def new(self):
        self.text.delete(1.0,END)
        
    def build_menu(self):
        # Build the menubars
        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="New", command=self.new)
        self.filemenu.add_command(label="Open", command=self.askopenfile)
        self.filemenu.add_command(label="Save", command=self.asksavefile)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", command=self.askexit)
        self.menubar.add_cascade(label="File", menu=self.filemenu)

        self.editmenu = Menu(self.menubar, tearoff=0)
        self.editmenu.add_command(label="Cut",command=self.cut)
        self.editmenu.add_command(label="Copy",command=self.copy)
        self.editmenu.add_command(label="Paste",command=self.paste)
        self.menubar.add_cascade(label="Edit",menu=self.editmenu)

    def __init__(self, parent=None, file=None):
        # Put it all together
        self.build_menu()
        self.text.pack(side=TOP)
        self.root.config(menu=self.menubar)
        self.root.title("Text Editor")
        self.root.mainloop()
            


te=TextEditor()
